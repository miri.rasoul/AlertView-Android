package ir.pkokabi.alertviewexample;

/**
 * Created by p.kokabi on 6/27/17.
 */

interface AlertViewCallBack {
    void onFinish();

    void onRefresh();
}
